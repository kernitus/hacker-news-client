package group1.project.data.queries;

public enum UserSortType {
    USERNAME, POST_COUNT, COMMENT_COUNT
}
