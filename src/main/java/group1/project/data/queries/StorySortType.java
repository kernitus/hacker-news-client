package group1.project.data.queries;

public enum StorySortType {
    TITLE, SCORE, TIME, COMMENT_COUNT
}
