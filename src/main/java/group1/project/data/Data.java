package group1.project.data;

public interface Data {

    /**
     * Loads relevant data from database to RAM
     */
    void load();
}
