# HackerNewsClient

Desktop client for HackerNews using Java Processing library. Loads news details and comments from exported JSON files into an embedded H2 database, which allows for easy searching and indexing. Also makes extensive use of Java 8 threads and callback lambdas to keep the interface responsive.


## Running

Simply execute `mvn clean compile exec:java` from the main project directory.

## Screenshots

### Sorting
Main screen, displays all stories and allows for sorting them by various criteria.

![Main screen](screenshots/sort.png)

### Searching
Searching through the stories by filtering as you type by either author or content.

![Search](screenshots/search.png)

### Comments
Shows story details and all comments in a scrollable view. Story link and usernames can be clicked on.

![Comments](screenshots/comments.png)

### User
User screen showing their stories, comments, and activity.

![User](screenshots/user.png)
